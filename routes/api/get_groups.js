var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var bcrypt = require('bcrypt-nodejs');
router.get('/get_groups',function(req,res){
    utils.getAllGroups(function(call) {
      if(call == 1) {
        res.send({"response" : 1, "message" : "Something went wrong, oops?"});
      } else {
        res.send(call);
      }
    });
});

module.exports = router;
