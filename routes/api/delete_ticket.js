var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var bcrypt = require('bcrypt-nodejs');

router.get('/delete_ticket/:id',function(req,res){
    utils.getTicketData(req.params.id, function(ticket_data) {
      if(ticket_data == 1) {
        res.send({"response" : 1, "message" : "Invalid ticket!"});
      } else {
        if(ticket_data.status != "Closed") {
          utils.getGroupByUUID(req.session.uuid, function(group) {
            if(group == 1) {
              res.send({"response" : 1, "message" : "Invalid uuid!"});
            } else {
              if(group.admin == true || ticket_data.author == req.session.uuid) {
                utils.deleteTicket(req.params.id, function(call) {
                  if(call == 1) {
                    res.send({"response" : 1, "message" : "Invalid id!"});
                  } else {
                    res.send({"response" : 0});
                  }
                });
              } else {
                res.send({"response" : 1, "message" : "Buddy, you can't cancel this ticket!"});
              }
            }
          });
        } else {
          res.send({"response" : 1, "message" : "Why are you trying to close a ticket thats already closed?"});
        }
      }
    });
});

module.exports = router;
