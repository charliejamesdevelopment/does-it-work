var config = require('../../config');
var year_start = config.years;
var year = function(dob) {
  var age = getAge(dob);
  var date = new Date(dob);
  var today = new Date()
  var new_date = new Date(date.getFullYear() + year_start.start_school_at_age, date.getMonth()+1, date.getDate());
  var new_age = today.getFullYear() - new_date.getFullYear();
  if(new_date.getMonth() > year_start.year_start_month || new_date.getMonth() == year_start.year_start_month && new_date.getDate() >= year_start.year_start_day) {
    new_age = new_age - 1;
  }
  return new_age;
}

var getAge = function(date) {
    var today = new Date();
    var birthDate = new Date(date);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}

module.exports = {
  year: year,
  getAge: getAge
}
