var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var bcrypt = require('bcrypt-nodejs');

router.get('/delete_appointment/:id',function(req,res){
    utils.getAppointmentData(req.params.id, function(appointment_data) {
      if(appointment_data == 1) {
        res.send({"response" : 1, "message" : "Invalid appointment!"});
      } else {
        utils.deleteAppointmentAdmin(req.params.id, function(call) {
          if(call == 1) {
            res.send({"response" : 1, "message" : "Invalid id!"});
          } else {
            res.send({"response" : 0});
          }
        });
      }
    });
});

module.exports = router;
