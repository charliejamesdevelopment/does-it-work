var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var config = require('../../config.js');
var appointment_utils = require('../utils/appointments.js');
var year_utils = require('../utils/year');
router.get('/',function(req,res){
  utils.isLoggedIn(req,function(call) {
    if(call == false) {
      res.redirect('/');
    } else {
      utils.getUserData(req.session.uuid, function(obj) {
        if(obj == 1) {
          console.log("Couldn't find data for " + req.session.uuid);
        } else {
          utils.getGroupByUUID(req.session.uuid, function(g) {
            utils.getChildrenByUUID(req.session.uuid, function(data) {
              res.render('main/children', {
                year_utils: year_utils,
                getStatus: appointment_utils.getStatus,
                getAppointmentStatuses: appointment_utils.getAppointmentStatuses,
                times: config.appointment.times,
                types: config.appointment.types,
                admin: g.admin,
                user: obj,
                group: g,
                data: data,
                active: "My Children",
              });
            });
          });
        }
      });
    }
  });
});

module.exports = router;
