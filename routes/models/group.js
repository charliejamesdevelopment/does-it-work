var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var groupSchema = new Schema({
  group_id: { type: Number, required: true },
  group_name: { type: String, required: true },
  permissions: { type: String, required: false },
  default: { type: Boolean, required: true },
  admin: { type: Boolean, required: true },
  updated_at: { type: Date, required: false },
  created_at: { type: Date, required: false }
});

groupSchema.pre('save', function(next) {
  // get the current date
  var currentDate = new Date();

  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;

  next();
});
groupSchema.set('collection', 'groups');

var User = mongoose.model('Group', groupSchema);

// make this available to our users in our Node applications
module.exports = User;
