var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ticketSchema = new Schema({
  author: { type: String, required: true},
  status: { type: String, required: true, default : "Open" },
  name: { type: String, required: true },
  description: { type: String, required: true },
  priority: { type: String, required: true, default : "Low"},
  type: { type: String, required: true },
  messages: { type: Array, required: true },
  created_at: Date,
  updated_at: Date
});

ticketSchema.pre('save', function(next) {

  this.active = 0;

  // get the current date
  var currentDate = new Date();

  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;

  next();
});
ticketSchema.set('collection', 'tickets');

var User = mongoose.model('Ticket', ticketSchema);

// make this available to our users in our Node applications
module.exports = User;
