var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var codeSchema = new Schema({
  code: { type: String, required: true },
  parent: { type: String, required: true },
  students: { type: Array, required: true },
  active: { type: Number, required: true },
  group: { type: Number, required: true },
  created_at: Date,
  updated_at: Date
});

codeSchema.pre('save', function(next) {

  this.active = 0;

  // get the current date
  var currentDate = new Date();

  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;

  next();
});
codeSchema.set('collection', 'codes');

var User = mongoose.model('Code', codeSchema);

// make this available to our users in our Node applications
module.exports = User;
